(function ($) {
    "use strict";
    jQuery(document).ready(function () {

        var elementorSections = $('.section'),
            hiddenclass = '.hidden-desktop, .hidden-tablet, .hidden-phone',
            typeArr = [],
            styleArr = [],
            hiddenIndexArr = [];

        elementorSections.each(function (index, element) {
            var element_class = $(element).attr('class').split(' '),
                type = $(element).data('type'),
                style = $(element).attr('style');

            typeArr.push(type);
            styleArr.push(style);

            if($(element).is(hiddenclass) === true){
                hiddenIndexArr.push(index);
            }
        });

        hiddenIndexArr.forEach(function(item){
            if(item === 0){
                typeArr.pop();
                styleArr.pop();
            } else {
                if(typeArr[item + 1] === 'color' && typeArr[item - 1] === 'color') {
                    if(styleArr[item + 1] === styleArr[item - 1]) {
                        typeArr.pop();
                        styleArr.pop();
                    } else {
                        typeArr.splice(typeArr.indexOf(item, 1));
                        styleArr.splice(styleArr.indexOf(item, 1));
                    }
                } else if(typeArr[item + 1] === 'image' && typeArr[item - 1] === 'image') {
                    typeArr.pop();
                    styleArr.pop();
                } else {
                    typeArr.splice(typeArr.indexOf(item, 1));
                    styleArr.splice(styleArr.indexOf(item, 1));
                }
            }
        });

        var i = 0;
        elementorSections.each(function (index, element) {
            if($(element).is(hiddenclass) === false){

                $(elementorSections[index]).attr('style', styleArr[i]);
                $(elementorSections[index]).attr('data-type', typeArr[i]);

                i++;
            }
        });

    });

})(jQuery);
